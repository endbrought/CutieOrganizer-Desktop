/*
* CutieOrganizer - for Desktop
*
* CutieOrganizer is an attempt to give a full agenda, calendar, contact
* manager.
*
* Copyright (C) 2018 Broken <broken.physicality@protonmail.com>
*
* This program is free software: you can redistribute it and/or modify it under
* the terms of the GNU General Public License as published by the Free Software
* Foundation, either version 3 of the License, or (at your option) any later
* version.
*
* This program is distributed in the hope that it will be useful, but WITHOUT ANY
* WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
* PARTICULAR PURPOSE.  See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along with
* this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "appointment.h"
#include "datetimeops.cpp"

Appointment::Appointment(QObject *parent)
  : QObject(parent), m_startTime (date_ops::roundDateToHalfHour(
                                    QDateTime::currentDateTime())
                                  ),
    m_stopTime (date_ops::roundDateToHalfHour(
                  QDateTime::currentDateTime().addSecs(1800))
                )
{

}

Appointment::Appointment(QDateTime startTime, QObject *parent)
  : QObject(parent), m_startTime (startTime),
    m_stopTime (startTime.addSecs(1800))
{

}

QString Appointment::name() const
{
  return m_name;
}

void Appointment::setName(const QString &name)
{
  if (name != m_name)
    {
      emit nameWillChange();
      emit nameIsChanging();
      m_name = name;
      emit nameHasChanged();
    }
}

QString Appointment::description() const
{
  return m_description;
}

void Appointment::setDescription(const QString &description)
{
  if (description != m_description)
    {
      emit descriptionWillChange();
      emit descriptionIsChanging();
      m_description = description;
      emit descriptionHasChanged();
    }
}

// todo: convert to Qt Location
QString Appointment::location() const
{
  return m_location;
}

// todo: convert to Qt Location
void Appointment::setLocation(const QString &location)
{
  if (location != m_location)
    {
      emit locationWillChange();
      emit locationisChanging();
      m_location = location;
      emit locationHasChanged();
    }
}

QDateTime Appointment::startTime() const
{
  return m_startTime;
}

void Appointment::setStartTime(const QDateTime &startTime)
{
  if ((startTime.toString("ddd MMMM d yy hh:mm") !=
      m_startTime.toString("ddd MMMM d yy hh:mm")) &&
      (startTime < m_stopTime)) // must be strictly before stop time no <=
    {
      emit startTimeWillChange();
      emit startTimeisChanging();
      m_startTime = startTime;
      emit startTimeHasChanged();
    }
}

QDateTime Appointment::stopTime() const
{
  return m_stopTime;
}

void Appointment::setStopTime(const QDateTime &stopTime)
{
  if ((stopTime.toString("ddd MMMM d yy hh:mm") !=
      m_stopTime.toString("ddd MMMM d yy hh:mm")) &&
      (stopTime > m_startTime)) // must be strictly after start time no >=
    {
      emit stopTimeWillChange();
      emit stopTimeisChanging();
      m_stopTime = stopTime;
      emit stopTimeHasChanged();
    }
}
