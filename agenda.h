/*
 * CutieOrganizer - for Desktop
 *
 * CutieOrganizer is an attempt to give a full agenda, calendar, contact
 * manager.
 *
 * Copyright (C) 2018 Broken <broken.physicality@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AGENDA_H
#define AGENDA_H

#include "appointment.h"

#include <QObject>
#include <QList>

class Agenda : public QObject
{
  Q_OBJECT
public:
  typedef QList<Appointment*> AppointmentList;
  explicit Agenda(QObject *parent = nullptr);

  AppointmentList appointment() const;
  void createAppointment();
  bool removeAppointment();

signals:
  void willCreateAppointment();
  void isCreatingAppointment();
  void hasCreatedAppointment();

  void willRemoveAppointment();
  void isRemovingAppointment();
  void hasRemovedAppointment();

public slots:

private:
   AppointmentList m_appointment;

};

#endif // AGENDA_H
