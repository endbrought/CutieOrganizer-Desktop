/*
 * CutieOrganizer - for Desktop
 *
 * CutieOrganizer is an attempt to give a full agenda, calendar, contact
 * manager.
 *
 * Copyright (C) 2018 Broken <broken.physicality@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <math.h>
#include <QDateTime>

namespace date_ops {
  QDateTime roundDateToHalfHour(const QDateTime &date) {
    QDateTime newDate(date);
    if (date.isValid())
      {
        int minutes = date.time().minute();
        int hours = date.time().hour();
        int toHalf = abs(30 - minutes); // time to half-hour

        // check if < 15 minutes -> current hour start
        if (minutes < 15)
          {
            QTime newTime(hours, 0, 0, 0);
            newDate.setTime(newTime);
          }
        // check if toHalf < 15 -> current hour half
        else if (toHalf < 15)
          {
            QTime newTime(hours, 30, 0, 0);
            newDate.setTime(newTime);
          }
        // return next hour start
        else if (hours == 23) // if next hour is next day, increment day
          {
            QTime newTime(0, 0, 0, 0);
            newDate = newDate.addDays(1);
            newDate.setTime(newTime);
          }
        else
          {
            QTime newTime(hours + 1, 0, 0, 0);
            newDate.setTime(newTime);
          }
      }
    return newDate;
  }
}
