/*
 * CutieOrganizer - for Desktop
 *
 * CutieOrganizer is an attempt to give a full agenda, calendar, contact
 * manager.
 *
 * Copyright (C) 2018 Broken <broken.physicality@protonmail.com>
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef APPOINTMENT_H
#define APPOINTMENT_H

#include <QObject>
#include <QDateTime>

class Appointment : public QObject
{
  Q_OBJECT
public:
  explicit Appointment(QObject *parent = nullptr);
  explicit Appointment(QDateTime startTime, QObject *parent = nullptr);


  QString name() const;
  void setName(const QString &name);

  QString description() const;
  void setDescription(const QString &description);

  QString location() const;
  void setLocation(const QString &location);

  QDateTime startTime() const;
  void setStartTime(const QDateTime &startTime);

  QDateTime stopTime() const;
  void setStopTime(const QDateTime &stopTime);

signals:
  void nameWillChange();
  void nameIsChanging();
  void nameHasChanged();

  void descriptionWillChange();
  void descriptionIsChanging();
  void descriptionHasChanged();

  void locationWillChange();
  void locationisChanging();
  void locationHasChanged();

  void startTimeWillChange();
  void startTimeisChanging();
  void startTimeHasChanged();

  void stopTimeWillChange();
  void stopTimeisChanging();
  void stopTimeHasChanged();

public slots:

private:
  QString m_name;
  QString m_description;
  QString m_location; // todo: convert to Qt location and implement map
  QDateTime m_startTime;
  QDateTime m_stopTime;
//  QList<QPointer<Contact>> m_participants; // todo: implement contact
};

#endif // APPOINTMENT_H
